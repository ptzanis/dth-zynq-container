# DTH ZYNQ Container 

The project offers a container image and tools for executing DTH Testbench tests within a container running on a ZYNQ platform. This container is designed with the following features:
- **Architecture:** It's built for the ALMA9-ARM64 architecture.
- **XDAQ Environment:** The container includes the complete XDAQ core+worksuite environment, utilizing the `cmsos/hub/cmsos-master-alma9-arm64-full` image as its base.
- **CI/CD Integration:** The project employs `kaniko` within a `k8s-arm` environment to build and publish the container image. Users have the flexibility to specify their requirements for `dnf` and `python` packages.
- **Container Registry:** Images are available through the project's Container Registry.
    - The `:latest` tag remains in sync with the master branch of the project.
    - Users can create versioned releases, such as `:1.0.0`, by pushing git tags.

## Table of Contents
[[_TOC_]]
## Developer Guide

### `dnf` Packages
Developer can add `dnf` packages inside the `extern/dnf-packages.txt` file. The `dnf` packages will be installed during the build process of the container image.

### `python` Packages
Developer can add `python` packages inside the `extern/python-packages.txt` file. The `python` packages will be installed during the build process of the container image.

### Container Image Build
The container image is built using `kaniko` within a `k8s-arm` environment. The build process is triggered by pushing a commit or by a git tag. In case of a commit, the `:latest` tag is updated. In case of a git tag, a new versioned tag is created. The tag should follow the `vX.Y.Z` format. For example, `1.0.0`.

## Pre-requisites
For running the container, the following tools are required:
- [podman](https://podman.io/getting-started/installation)
- [podman-compose](https://docs.podman.io/en/latest/markdown/podman-compose.1.html)

User can also use `docker` and `docker-compose` instead of `podman` and `podman-compose` respectively.

## Installation
Clone the repository:

```bash
git clone https://gitlab.cern.ch/ptzanis/dth-zynq-container.git
cd dth-zynq-container
```

## Usage

Notice: Use the corresponding `IMAGE_VERSION` to run the container with a specific version of the image. If the `IMAGE_VERSION` is not specified, the `:latest` tag will be used.

Initialize the container with the following command:

```bash
sudo IMAGE_VERSION=1.0.0 podman-compose -f docker/compose-file.yml up -d
```
Use can interact with the container and execute any command inside the container using the following command:
```bash
tools/run-inside-container.sh 'cd DTH_Script/TestBench/; ls'
```
Or the usual `podman/docker` command:  `sudo podman exec -it dth-zynq-xdaq-container /bin/bash -c "cd DTH_Script/TestBench/; ls"`:

Stop the container with the following command:

```bash
sudo IMAGE_VERSION=1.0.0 podman-compose -f docker/compose-file.yml down
```

