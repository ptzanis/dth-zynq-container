FROM gitlab-registry.cern.ch/cmsos/hub/cmsos-master-alma9-arm64-full:0.0.0.2

RUN dnf -y groupupdate cmsos_core cmsos_worksuite
ENV XDAQ_ROOT=/opt/xdaq
ENV XDAQ_SETUP_ROOT=/opt/xdaq/share
ENV XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
ENV LD_LIBRARY_PATH=/opt/xdaq/lib:/opt/xdaq/lib64

WORKDIR /
COPY tools/entrypoint.sh .
RUN chmod +x /entrypoint.sh

COPY extern/requirements* .
RUN dnf -y install $(cat requirements-dnf.txt)

RUN pip3 install /opt/xdaq/etc/PyHAL/*.whl -r requirements-python.txt